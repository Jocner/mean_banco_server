// Rutas para crear usuarios
const express = require('express');
const router = express.Router();
const operacionesController = require('../controllers/operacionesController');
const auth = require('../middleware/auth');
const { check } = require('express-validator');

// Crea un usuario
// api/transferencias
router.post('/', 
    auth,
    [
        check('montotransacion', 'El monto es obligatorio').not().isEmpty(),
        check('ruttercero', 'El monto es obligatorio').not().isEmpty()
      
    ],
    operacionesController.transferencia
);


module.exports = router;