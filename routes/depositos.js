// Rutas para crear usuarios
const express = require('express');
const router = express.Router();
const operacionesController = require('../controllers/operacionesController');
const auth = require('../middleware/auth');
const { check } = require('express-validator');

// Crea un usuario
// api/deposito
router.post('/', 
    auth,
    [
        check('montotransacion', 'El monto es obligatorio').not().isEmpty()
      
    ],
    operacionesController.cargarSaldo
);


module.exports = router;